import Joi from 'joi';

class Validation {
  /**
   * @description Validation using Joi for Image schema
   * @param {Object} Image
   */
  validateImage = (image) => {
    const validateObject = Joi.object({
      callback_url: Joi.string().required(),
      instruction: Joi.string().required(),
      attachment_type: Joi.string().required(),
      attachment: Joi.string().required(),
      objects_to_annotate: Joi.array().items(Joi.string()).min(1).required(),
      with_labels: Joi.boolean().required(),
      urgency: Joi.string().required(),
      isAnnotated: Joi.boolean(),
      priority: Joi.number(),
    });

    return validateObject.validateAsync(image);
  };
}

export default Validation;
