import mongoose from 'mongoose';

import 'dotenv/config.js';
import logger from './logger.js';
// mongodb://localhost:27017/annotations-app
const mongoDbUrl = process.env.MONGODB_URL;

// MongoDB Connection
mongoose
  .connect(mongoDbUrl, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
  })
  .then(() => console.log(`Connected to MongoDB ${mongoDbUrl}`))
  .catch((error) => {
    console.error('Could not Connect to MongoDB...', error);
    logger.error('Could not Connect to MongoDB...', error);
    process.exit(1);
  });

export default mongoose;
