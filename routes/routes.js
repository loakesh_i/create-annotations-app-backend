import express from 'express';
const router = express.Router();
import ImageController from '../controllers/images.js';

const {
  addImage,
  getImagesToAnnotate,
  updateImageWithAnnotations,
} = new ImageController();

// POST route to upload the details
router.post('/api/send-images-to-annotate', addImage);

// GET route to get all the images to be annotated
router.get('/api/get-images-to-annotate', getImagesToAnnotate);

// PUT route to update images with the annotated data
router.put('/api/update-image-with-annotations/:id', updateImageWithAnnotations);
  
export default router;
