import ImageModel from '../models/image.js';
import ErrorResponse from '../utility/errorResponse.js';
import RabbitMQ from '../msgQueue/rabbitMQ.js';

const { sendToQueue, consumeFromQueue } = new RabbitMQ();
const {
  createImage,
  getAllImages,
  findOne,
  saveImage,
  updateImage,
  findMultipleImages,
  getAllImagesToAnnotate,
} = new ImageModel();

class ImageService {
  addNewImage = async (data) => {
    const isImagePresent = await findOne({
      attachment: data.attachment,
    });

    // checking if the image is already present and it should not be annotated yet
    if (isImagePresent) {
      if (!isImagePresent.isAnnotated) {
        throw new ErrorResponse('Image already exists', 409);
      }
    }
    return createImage(data);
  };

  imagesToBeAnnotated = async (data) => {
    const imagesToBeAnnotated = await getAllImagesToAnnotate();

    if (imagesToBeAnnotated.length == 0) {
      throw new ErrorResponse('No Images to Annotate, you can add more', 409);
    }
    return imagesToBeAnnotated;
  };

  updateAnnotations = async (id, data) => {
    const updatedImage = await updateImage(id, data);
    if (updatedImage instanceof Error) {
      throw new ErrorResponse(error.message, 400);
    }

    const message = `Please find the annotated image details ${updatedImage}`;

    await sendToQueue({
      emailId: updatedImage.callback_url,
      subject: 'Annotated Image',
      message,
    });
    await consumeFromQueue();
    return updatedImage;
  };
}

export default ImageService;
