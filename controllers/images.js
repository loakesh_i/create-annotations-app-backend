import ImageService from '../services/imageService.js';
import ErrorResponse from '../utility/errorResponse.js';
import Validation from '../helpers/validation.js';
import logger from '../config/logger.js';

const { validateImage } = new Validation();
const { addNewImage, imagesToBeAnnotated, updateAnnotations } = new ImageService();

class ImageController {
  /**
   * @description Validates request body and adds images
   * @route POST /api/send-images-to-annotate
   * @param {Object} req
   * @param {Object} res
   */
  addImage = async (req, res) => {
    const responseData = {};
    try {
      let setPriority;
      switch (req.body.urgency) {
        case 'IMMEDIATE':
          setPriority = 0;
          break;
        case 'DAY':
          setPriority = 1;
          break;
        case 'WEEK':
          setPriority = 2;
          break;
        default:
          setPriority = 3;
          break;
      }
      const imageObject = {
        callback_url: req.body.callback_url,
        instruction: req.body.instruction,
        attachment_type: req.body.attachment_type,
        attachment: req.body.attachment,
        objects_to_annotate: req.body.objects_to_annotate,
        with_labels: req.body.with_labels,
        urgency: req.body.urgency,
        priority: setPriority,
      };
      const { error } = await validateImage(imageObject);
      if (error) {
        throw new Error(error);
      }
      const result = await addNewImage(imageObject);
      responseData.success = true;
      responseData.message = 'Successfully Added Image!';
      responseData.data = result;
      logger.info(responseData.message);
      res.status(200).send(responseData);
    } catch (error) {
      responseData.success = false;
      responseData.message = error.message;
      logger.error(error.message);
      res.status(error.statusCode || 500).send(responseData);
    }
  };

  /**
   * @description Gets all the images to annotate
   * @route GET /api/get-images-to-annotate
   * @param {Object} req
   * @param {Object} res
   */
  getImagesToAnnotate = async (req, res) => {
    const responseData = {};
    try {
      const result = await imagesToBeAnnotated();
      responseData.success = true;
      responseData.message = 'All images to be annotated';
      responseData.data = result;
      logger.info(responseData.message);
      res.status(200).send(responseData);
    } catch (error) {
      responseData.success = false;
      responseData.message = error.message;
      logger.error(error.message);
      res.status(error.statusCode || 500).send(responseData);
    }
  };

  /**
   * @description Updates image with the annotations
   * @route PUT /api/update-image-with-annotations/:id
   * @param {Object} req
   * @param {Object} res
   */
  updateImageWithAnnotations = async (req, res) => {
    const responseData = {};
    try {
      const result = await updateAnnotations(req.params.id, req.body);
      responseData.success = true;
      responseData.message = 'Updated image successfully';
      responseData.data = result;
      logger.info(responseData.message);
      res.status(200).send(responseData);
    } catch (error) {
      responseData.success = false;
      responseData.message = error.message;
      logger.error(error.message);
      res.status(error.statusCode || 500).send(responseData);
    }
  };
}

export default ImageController;
