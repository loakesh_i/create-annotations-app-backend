import mongoose from 'mongoose';
const { Schema } = mongoose;

const imageSchema = new Schema(
  {
    callback_url: {
      type: String,
      required: true,
    },
    instruction: {
      type: String,
      required: true,
    },
    attachment_type: {
      type: String,
      enum: ['IMAGE'],
      required: true,
    },
    attachment: {
      type: String,
      required: true,
    },
    objects_to_annotate: {
      type: [String],
    },
    with_labels: {
      type: Boolean,
      default: true,
    },
    urgency: {
      type: String,
      enum: ['IMMEDIATE', 'DAY', 'WEEK', 'MONTH'],
    },
    isAnnotated: {
      type: Boolean,
      default: false,
    },
    isEmailSent: {
      type: Boolean,
      default: false,
    },
    annotations: {
      type: Object,
    },
    errorMessage: {
      type: String,
    },
    priority: {
      type: Number,
    },
  },
  {
    timestamps: true,
    strict: true,
  }
);

const Image = mongoose.model('Image', imageSchema);

class ImageModel {
  createImage = (data) => {
    return Image.create({
      callback_url: data.callback_url,
      instruction: data.instruction,
      attachment_type: data.attachment_type,
      attachment: data.attachment,
      objects_to_annotate: data.objects_to_annotate,
      with_labels: data.with_labels,
      urgency: data.urgency,
      isAnnotated: data.isAnnotated,
      annotations: data.annotations,
      isEmailSent: data.isEmailSent,
      errorMessage: data.errorMessage,
      priority: data.priority,
    });
  };

  getAllImagesToAnnotate = () => {
    return Image.find({ isAnnotated: false });
  };

  getAllImages = () => {
    return Image.find();
  };

  findOne = (fields) => {
    return Image.findOne(fields);
  };

  saveImage = (image) => {
    return image.save;
  };

  updateImage = (id, updatedImageObject) => {
    console.log(id, JSON.stringify(updatedImageObject));
    return Image.findOneAndUpdate(
      { _id: id },
      {
        $set: updatedImageObject,
      },
      {
        new: true,
        // useFindAndModify: false,
      }
    );
  };

  findMultipleImages = (fields) => {
    return Image.find(fields);
  };
}

export default ImageModel;
